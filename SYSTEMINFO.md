# Incognito Soap

Vendor: Incognito
Homepage: https://www.incognito.com/

Product: Incognito
Product Page: https://www.incognito.com/

## Introduction
We classify Incognito Soap into the Network Services domain as Incognito provides network-related services and functionalities.

## Why Integrate
The Incognito Soap adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Incognito. With this adapter you have the ability to perform operations on items such as:

- Device
- IP
- Subnet
- VPN

## Additional Product Documentation
The [API documents for Incognito Soap]()
# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Incognito_soap System. The API that was used to build the adapter for Incognito_soap is usually available in the report directory of this adapter. The adapter utilizes the Incognito_soap API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Incognito Soap adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Incognito. With this adapter you have the ability to perform operations on items such as:

- Device
- IP
- Subnet
- VPN

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 

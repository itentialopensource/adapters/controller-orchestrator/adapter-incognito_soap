
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:06PM

See merge request itentialopensource/adapters/adapter-incognito_soap!12

---

## 0.3.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-incognito_soap!10

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:13PM

See merge request itentialopensource/adapters/adapter-incognito_soap!9

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:26PM

See merge request itentialopensource/adapters/adapter-incognito_soap!8

---

## 0.3.0 [07-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito_soap!7

---

## 0.2.4 [03-28-2024]

* Changes made at 2024.03.28_13:35PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito_soap!5

---

## 0.2.3 [03-11-2024]

* Changes made at 2024.03.11_15:55PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito_soap!4

---

## 0.2.2 [02-28-2024]

* Changes made at 2024.02.28_11:23AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito_soap!3

---

## 0.2.1 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito_soap!2

---

## 0.2.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-incognito_soap!1

---

## 0.1.3 [06-10-2022]

* Bug fixes and performance improvements

See commit ea50681

---

## 0.1.2 [06-09-2022] & 0.1.1 [06-08-2022]

- Initial Commit

See commit 62593ab

---

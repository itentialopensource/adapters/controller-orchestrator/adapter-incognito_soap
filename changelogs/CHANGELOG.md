
## 0.1.3 [06-10-2022]

* Bug fixes and performance improvements

See commit ea50681

---

## 0.1.2 [06-09-2022] & 0.1.1 [06-08-2022]

- Initial Commit

See commit 62593ab

---
